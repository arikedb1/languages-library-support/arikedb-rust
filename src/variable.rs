use crate::variable_type::VariableType;
use crate::epoch::Epoch;

pub struct Variable {
    pub name: String,
    pub vtype: VariableType,
    pub buffer_size: u32,
}


pub struct DataPoint {
    pub name: String,
    pub vtype: VariableType,
    pub timestamp: String,
    pub epoch: Epoch,
    pub value: String,
}
