
pub enum Event {
    OnSet = 0,
    OnChange = 1,
    OnRise = 2,
    OnFall = 3,
    OnValueReachVal = 4,
    OnValueEqVal = 5,
    OnValueLeaveVal = 6,
    OnValueDiffVal = 7,
    OnCrossHighLimit = 8,
    OnCrossLowLimit = 9,
    OnOverHighLimit = 10,
    OnUnderLowLimit = 11,
    OnValueReachRange = 12,
    OnValueInRange = 13,
    OnValueLeaveRange = 14,
    OnValueOutRange = 15,
}


pub struct VarEvent {
    pub event: Event,
    pub value: String,
    pub low_limit: String,
    pub high_limit: String,
}
