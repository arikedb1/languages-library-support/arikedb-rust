pub enum Epoch {
    Second,
    Millisecond,
    Microsecond,
    Nanosecond
}