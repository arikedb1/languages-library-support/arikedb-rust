use arikedb::{epoch::Epoch, event::{Event, VarEvent}, variable::Variable, variable_type::VariableType, ArikedbClient };


#[tokio::main]
async fn main () {

    let mut client = ArikedbClient::connect("127.0.0.1", 50051).await.unwrap();

    match client.authenticate(&"admin".to_string(), &"admin".to_string()).await {
        Ok(msg) => {
            println!("Code: {}", msg);
            println!("Token: {}", msg);
        },
        Err(err) => println!("Error: {}", err),
    }

    for i in 0..110 {
        let list = client.list_collections().await.unwrap();

        for collection in list {
            println!("Collection: {}", collection.name);
        }

        tokio::time::sleep(tokio::time::Duration::from_millis(3000)).await;
    }

    // let list = client.list_variables(&collName).await.unwrap();
    // let var_names = vec![varName.clone()];
    // let points = client.get_variables(&collName, vec![varName.clone()], 0, Epoch::Nanosecond).await.unwrap();
    
    // for point in points {
    //     println!("Variable: {} {} {}", point.name, point.timestamp, point.value);
    // }

    // let mut coll_names = vec![];

    // for i in 0..3 {
    //     coll_names.push(format!("Collection_{}", i));
    // }

    // client.create_collections(&coll_names).await.unwrap();
    
    // let mut x = 0;
    // for coll in &coll_names {
    //     let mut variables = vec![];
        
    //     for i in 0..100 {
    //         variables.push(Variable {
    //             name: format!("Variable_{}_{}", x, i),
    //             vtype: VariableType::F64,
    //             buffer_size: 10,
    //         });
    //     }
    //     x += 1;

    //     client.create_variables(&coll, variables).await.unwrap();
        
    //     // let vars = client.list_variables(&coll).await.unwrap();
    //     // for var in vars {
    //     //     println!("{} ", var.name);
    //     // }

    // }

    // let events = vec![
    //     VarEvent {
    //         event: Event::OnRise,
    //         value: "".to_string(),
    //         low_limit: "0".to_string(),
    //         high_limit: "100".to_string(),
    //     }
    // ];

    // let h = client.subscribe_variables(
    //     &collName,
    //     vec![varName.clone()],
    //     events,
    //     |_point| -> () {
    //         println!("Variable: {} {} {}", _point.name, _point.timestamp, _point.value);
    //     }
    // ).await;

    // h.unwrap().await.unwrap();

}
